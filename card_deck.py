import random as r


class Card(object):
    def __init__(self, category, value):
        self.category = category
        self.value = value

    def __str__(self):
        if self.category == 1:
            if self.value == 11:
                return 'jack of hearts'
            elif self.value == 12:
                return 'queen of hearts'
            elif self.value == 13:
                return 'king of hearts'
            elif self.value == 1:
                return 'ace of hearts'
            else:
                return '{} of hearts'.format(self.value)

        elif self.category == 2:
            if self.value == 11:
                return 'jack of spades'
            elif self.value == 12:
                return 'queen of spades'
            elif self.value == 13:
                return 'king of spades'
            elif self.value == 1:
                return 'ace of spades'
            else:
                return '{} of spades'.format(self.value)
        elif self.category == 3:
            if self.value == 11:
                return 'jack of clubes'
            elif self.value == 12:
                return 'queen of clubes'
            elif self.value == 13:
                return 'king of clubes'
            elif self.value == 1:
                return 'ace of clubes'
            else:
                return '{} of clubes'.format(self.value)
        else:
            if self.value == 11:
                return 'jack of diamonds'
            elif self.value == 12:
                return 'queen of diamonds'
            elif self.value == 13:
                return 'king of diamonds'
            elif self.value == 1:
                return 'ace of diamonds'
            else:
                return '{} of diamonds'.format(self.value)

    def __repr__(self):
        if self.category == 1:
            if self.value == 11:
                return 'jack of hearts'
            elif self.value == 12:
                return 'queen of hearts'
            elif self.value == 13:
                return 'king of hearts'
            elif self.value == 1:
                return 'ace of hearts'
            else:
                return '{} of hearts'.format(self.value)

        elif self.category == 2:
            if self.value == 11:
                return 'jack of spades'
            elif self.value == 12:
                return 'queen of spades'
            elif self.value == 13:
                return 'king of spades'
            elif self.value == 1:
                return 'ace of spades'
            else:
                return '{} of spades'.format(self.value)
        elif self.category == 3:
            if self.value == 11:
                return 'jack of clubes'
            elif self.value == 12:
                return 'queen of clubes'
            elif self.value == 13:
                return 'king of clubes'
            elif self.value == 1:
                return 'ace of clubes'
            else:
                return '{} of clubes'.format(self.value)
        else:
            if self.value == 11:
                return 'jack of diamonds'
            elif self.value == 12:
                return 'queen of diamonds'
            elif self.value == 13:
                return 'king of diamonds'
            elif self.value == 1:
                return 'ace of diamonds'
            else:
                return '{} of diamonds'.format(self.value)


class Deck(object):
    deck = []

    def __init__(self):
        for n in range(1, 5):
            for m in range(1, 14):
                self.deck.append(Card(n, m))

    def __str__(self):
        return "Deck length: {}".format(len(self.deck))

    def shuffle(self):
        cards_left = 52
        new_deck = []
        while cards_left > 0:
            card_index = r.randint(0, cards_left-1)
            new_deck.append(self.deck.pop(card_index))
            cards_left -= 1

        self.deck = new_deck

    def draw(self):
        if len(self.deck) > 0:
            return self.deck.pop(0)
        else:
            return 0  # make new deck

