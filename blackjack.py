from card_deck import Deck
from game import Game


def ask_for_players():
    number_of_players = 0
    is_valid = False
    while not is_valid:
        try:
            number_of_players = int(raw_input('number of players: '))
        except:
            print 'invalid input'
            is_valid = False
        else:
            if 0 < number_of_players < 7:
                is_valid = True
            else:
                print 'number of players must be between 1 and 6'
                is_valid = False
    return number_of_players


def house_ai(house):
    if house.value < 17:
        return 'h'
    else:
        return 's'


def find_winner(game):
    # players done playing, find winner
    winner_value = 0
    winner_names = []
    for player in game.players:
        if player.busted:
            pass
        else:
            if player.value >= winner_value:
                print 'winner value changed to: {}'.format(winner_value)
                winner_value = player.value

                if player.value == winner_value:
                    winner_names.append(player.name)
                    print 'appending {} to winning list with value {} '.format(player.name, player.value)

                else:
                    winner_names = [player.name]
                    print 'resetting list with {}'.format(winner_names)
    return winner_value, winner_names


def start():
    number_of_players = ask_for_players()
    game = Game(number_of_players, Deck())
    game.deal()

    for player in game.players:
        hit = True
        while hit:
            if player.busted:
                print '{} has busted with a value of: {}'.format(player.name, player.value)
                hit = False
                break
            else:
                if player.name == 'House':
                    print 'player: {} , hand: {} , value: {}'.format(player.name, player.cards, player.value)
                    from_user = house_ai(player)

                else:
                    print 'player: {} , hand: {} , value: {}'.format(player.name, player.cards, player.value)
                    from_user = raw_input('(h)it or (s)tay: ')

                if from_user == 'h':
                    hit = True
                    game.draw(player)
                    game.check_hand(player)
                else:
                    hit = False
    winner = find_winner(game)
    print 'winner: {} '.format(winner)


play_again = True
while play_again:
    start()
    choice = raw_input('play again(y/n): ')
    if choice == 'n':
        play_again = False
    else:
        play_again = True
