from card_deck import Deck


class Player(object):
    def __init__(self, name):
        self.name = name
        self.cards = []
        self.value = 0
        self.busted = False


class Game(object):

    def __init__(self, number_of_players, deck):
        self.number_of_players = number_of_players
        self.deck = deck
        self.deck.shuffle()
        self.is_won = False
        self.current_player = 1
        self.players = [Player('House')]
        for num in range(1, self.number_of_players + 1):
            name_of_player = raw_input('name of {}. player: '.format(num))
            self.players.append(Player(name_of_player))


    def check_hand(self, player):
        value = 0
        for card in player.cards:
            if card.value > 10:
                value += 10
            else:
                value += card.value

        if value > 21:
            player.busted = True
            return False
        else:
            return True

    def draw(self, player):
        drawn_card = self.deck.draw()
        player.cards.append(drawn_card)
        if drawn_card.value > 10:
            player.value += 10
        else:
            player.value += drawn_card.value

    def deal(self):
        for k in range(0, 2):
            for player in self.players:
                self.draw(player)

    def __str__(self):
        player_names = []
        for player in self.players:
            player_names.append(player.name)
        return "number of players: {}, cards in deck: {}, players: {}".format(self.number_of_players + 1, len(self.deck.deck), player_names)


